<a href="index.php">KEMBALI</a>
	<br/>
	<br/>
	<h3>TAMBAH DATA USER</h3>
	<form method="post" action="tambah_user.php">
		<table class="table">
			<tr>			
				<td>Nama Depan</td>
				<td><input type="text" name="nama_depan" class="form-control"></td>
			</tr>
            <tr>			
				<td>Nama Belakang</td>
				<td><input type="text" name="nama_belakang" class="form-control"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email"class="form-control" name="email"></td>
			</tr>
            <tr>
				<td>Password</td>
				<td><input type="text"class="form-control" name="password"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" name="alamat" class="form-control"></td>
			</tr>
            <tr>
				<td>Jabatan</td>
				<td><input type="text" name="jabatan" class="form-control"></td>
			</tr>
            <tr>
                <td>Role</td>
                <td>
                    <select name="role_access_id" class="form-control">
                        <option value="">Pilih Role</option>
                    <?php 
                include '../koneksi.php';
                $data = mysqli_query($koneksi,"select * from roles");
                while($d = mysqli_fetch_array($data)){
			    ?>
                <option value="<?php echo $d['role_access_id']; ?>"><?php echo $d['keterangan']; ?></option>
                <?php } ?>
                </select>
                </td>
            </tr>
			<tr>
				<td></td>
				<td><input type="submit" class="btn btn-primary" value="SIMPAN"></td>
			</tr>		
		</table>
	</form>