<?php
$page = (isset($_GET['page']))? $_GET['page'] : '';

switch($page){
  case 'home': // $page == home (jika isi dari $page adalah home)
  include "../admin/home.php"; // load file home.php yang ada di folder views
  break;
  
  case 'user': // $page == berita (jika isi dari $page adalah berita)
  include "../admin/user.php"; // load file berita.php yang ada di folder views
  break;
  
  case 'user.create': // $page == tentang (jika isi dari $page adalah tentang)
  include "../admin/create_user.php"; // load file tentang.php yang ada di folder views
  break;
  
  case 'user.edit':
  include "../admin/edit_user.php";
  break;
  
  // case 'case_selanjutnya':
  // include "views/case_selanjutnya.php";
  // break;
  
  // case 'case_selanjutnya':
  // include "views/case_selanjutnya.php";
  // break;
  
  default: // Ini untuk set default jika isi dari $page tidak ada pada 3 kondisi diatas
  include "../admin/home.php";
}
?>