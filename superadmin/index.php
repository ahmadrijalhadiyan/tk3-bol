<!DOCTYPE html>
<html>
<head>
	<title>Super Admin</title>
</head>
<body>
	<h2>Halaman Superadmin</h2>
	
	<br/>
	<!-- cek apakah sudah login -->
	<?php 
	session_start();
	// cek apakah yang mengakses halaman ini sudah login
	if($_SESSION['level']==""){
		header("location:index.php?pesan=gagal");
	}
	?>
	<h4>Selamat datang, <?php echo $_SESSION['username']; ?>! anda telah login.</h4>
	<br/>
	<br/>
	<a href="../logout.php">LOGOUT</a>
</body>
</html>